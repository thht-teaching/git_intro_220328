# Welcome to the introduction to git, gitlab, github....

Please read and apply the following instructions before coming to class on monday. In order to have an efficient lecture, it is important that you come with a preprared computer.

Instructions are going to be fairly short and should work straight forward. If anything does not work, please get in contact with me (thomas.hartmann@plus.ac.at) as soon as possible and I am going to help you fix the problem.

Please note that you are also going to find the slides in this repository. Check the `slides` folder.

# What software do you need?

No matter if you run Linux, Windows or Mac, you are going to need to install the following software. Everything we use runs on all systems, although installation is going to be different.

Here is what you need:

1. [git](https://git-scm.com/)
2. [Matlab](https://im.sbg.ac.at/pages/viewpage.action?pageId=42173904#Software(Stud.)-Matlab)

The installation of Matlab is quite straight forward. Just follow the [instructions](https://im.sbg.ac.at/download/attachments/42173904/TAH_Studierende_Installationsanleitung.pdf?version=1&modificationDate=1552390297000&api=v2).

The process of installing git depends on you operating system.

## Linux
Please use your package manager to install git.

## Windows
Please download and install it from [here](https://git-scm.com/download/win)

When installing, it will ask you whether you want to use git only from `bash` or also from the windows command line. Please choose the `only bash` option.

## Mac
Use your favourite method as mentioned [here](https://git-scm.com/download/mac)